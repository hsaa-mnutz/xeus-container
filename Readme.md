# Notebook Container

This container provides an instance of Jupyter Notebook and comes with all prerequisites already installed.

## Getting Started

These instructions will cover usage information and for the docker container 

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Open any command line and type `docker-compose up` to start the container.
Open the link which is printed in the command line by clicking or copying it.

The default access token is set to `letmein`. Assuming you did not change it, you can access the server by using `http://127.0.0.1:8888/?token=letmein`

The container can be started detached as a service by using the `-d` flag. In this case the url will not be printed. You can get a list of running notebook servers by querying the container

`docker exec -it notebook-container jupyter notebook list`

By using the environment variable `JUPYTER_TOKEN` you can set a custom access token.

## Authors

* **Matthias Nutz** - *Initial work and maintainer*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.