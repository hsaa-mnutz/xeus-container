FROM jupyter/minimal-notebook:latest

RUN conda update -n base conda

RUN conda install xeus-cling -c conda-forge -y --quiet

RUN fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN conda install -y jupyter_contrib_nbextensions

WORKDIR $HOME/work